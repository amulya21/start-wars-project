import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import LoggedIn from './components/LoggedIn'
import registerServiceWorker from './registerServiceWorker';
import {Router, Route, hashHistory} from 'react-router'
import Woops404 from './components/Woops404'


ReactDOM.render(<Router history = {hashHistory}>
					<Route path = "/" component={App}/>
					<Route path = "login" component={LoggedIn}/>
					<Route path = "*" component={Woops404}/>
				</Router>

				, document.getElementById('root'));
			registerServiceWorker();

//// ReactDOM.render(<Router history = {hashHistory}>
// 					<Route path = "/" component={App}/>
//  					<Route path = "list-days" component={App}/>
// 					<Route path = "add-day" component={App}/>
// 					<Route path = "*" component={Woops404}/>
// 				</Router>


//  	,document.getElementById('root'));

//  	registerServiceWorker();

// <div className="App">
//       	{(this.props.location.pathname === "/") ?
//       		<LoggedIn /> : 
//       			<SignIn />
//       		}
//       </div>

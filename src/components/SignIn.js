import React, { Component } from 'react';
import $ from "jquery"
import swal from "sweetalert"
import Cookies from 'universal-cookie';
import {Router, Route, hashHistory} from 'react-router'
import { withRouter } from 'react-router-dom';
import { browserHistory } from 'react-router';




class SignIn extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			username:'',
			password:''
		}
	}

	formSubmit = (e) => {
		e.preventDefault()
		let data = $(e.target).serializeArray();
		console.log(data);
		let objectToSend = {};


	};

	handleEmailChange = (e) => {
      this.setState({username: e.target.value});
};
	handlePasswordChange = (e) =>{
   this.setState({password: e.target.value});
};

	handleLogin = () =>{
		if(this.state.username === "Luke Skywalker"  && this.state.password === "19BBY"){
    	console.log("EMail: " + this.state.username);
    	console.log("Password: " + this.state.password);
    	swal({
                type: "success",
                title: `logged in`,
            }, function(){
                	console.log("okay clicked");
                  const cookies = new Cookies();
                  cookies.set("onboarded", true);
                  console.log(cookies.get("onboarded"));
                  browserHistory.push('/login');
                  //this.props.history.push("/login");
                })
    }else{
    	swal({
                type: "error",
                title: `incorrect username and password`
            })
    }
}

	render(){
		return(
				<form onSubmit={this.formSubmit}>
  					<div className="form-group">
    					<label htmlFor="exampleInputEmail1">Username</label>
    						<input type="text" className="form-control" id="exampleInputEmail1" 
    						aria-describedby="emailHelp" placeholder="Enter email" onChange={this.handleEmailChange}/>
    						<small id="emailHelp" 
    						className="form-text text-muted">We'll never share your username with anyone else.</small>
  						</div>
  					<div className="form-group">
    					<label htmlFor="exampleInputPassword1">Password</label>
    					<input type="password" className="form-control" id="exampleInputPassword1" 
    					placeholder="Password" onChange={this.handlePasswordChange} />
  					</div>
  					<button type="submit" className="btn btn-primary" onClick={this.handleLogin}>Submit</button>
				</form>
		)
	}

}

export default SignIn;
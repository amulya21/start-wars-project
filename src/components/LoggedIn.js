import React, { Component } from 'react';
import $ from "jquery"
import swal from "sweetalert"
import Request from "superagent"

class LoggedIn extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			textValue: '',
			urlAppend: ''

		}
	}
	componentDidMount(){

		console.log("componentDidmount is called");
		var url = "https://swapi.co/api/" + this.state.urlAppend;
		var _this = this;
		Request.get(url).then((response) =>{
			_this.setState({
				username: response
			});
			console.log();
		})

		
	}

	// componentDidMount(){


	// 	fetch("https://swapi.co/api/").then(results => {
	// 		return results.json()
	// 	}).then(data =>{
	// 		console.log(data)''
	// 	})
	// }

	handleChange =(e) => {
		this.setState({
			urlAppend: e.target.value
		})

	}

	render(){
		console.log("Logged component is called")

		return(
			<form onSubmit={this.formSubmit}>
                <div className="form-group">
                    <input className="form-control" ref="form" type="text"
                        name="listName" onChange= {this.handleChange} defaultValue={null} placeholder="star-wars search ....." required />
                </div>
                <button type="submit" className="btn btn-success">
                    <span className="fa fa-plus"></span> Add a list
                </button>
            </form>
			);
	}
}

export default LoggedIn;
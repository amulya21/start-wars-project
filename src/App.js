import React, { Component } from 'react';
import SignIn from './components/SignIn';
import LoggedIn from './components/LoggedIn';
import './App.css';
import Cookies from 'universal-cookie';

class App extends Component {
	constructor(props){
		super(props);
		this.state = {
			username:'',
			password:''
		}
	}
  render() {
  	console.log(this.props.location.pathname);
    return (
      <div className="App">
      	{(this.props.location.pathname === "/") ?
      		<SignIn /> : 
      		<LoggedIn/>
      		}
      </div>
    );
  }
}

export default App;
